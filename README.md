Curso Alura


Scripts:


CI/CD:

"test-ci": "ng test --watch=false --reporters junit --browsers ChromeHeadless",


Code Coverage:

"test-coverage": "ng test --watch=false --sourceMap=true --codeCoverage=true --browsers ChromeHeadless",


Biblioteca padrão JUnit Reporter:

npm i -D karma-junit-reporter@2.0.1


Relatório completo do code coverage ficará em coverage/nome-do-projeto/...