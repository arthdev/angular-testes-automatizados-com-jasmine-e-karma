import { UniqueIdService } from "./unique-id.service";

describe(UniqueIdService.name, () => {
  let service: UniqueIdService = null;

  beforeEach(() => {
    service = new UniqueIdService();
  });

  it(`${UniqueIdService.prototype.generateUniqueIdWithPrefix.name} SHOULD generate id WHEN called with prefix`, () => {
    const id = service.generateUniqueIdWithPrefix('app');
    expect(id).toContain('app-');
  });
  
  it(`${UniqueIdService.prototype.generateUniqueIdWithPrefix.name} SHOULD NOT generate duplicate id WHEN called multiple times`, () => {
    const ids = new Set();
    for (let i = 0; i < 50; i++) {
      ids.add(service.generateUniqueIdWithPrefix('app'));
    }
    expect(ids.size).toBe(50);
  });
  
  it(`#${UniqueIdService.prototype.getNumberOfGeneratedUniqueIds.name} shoudl return the number of generatedIds when calls`, () => {
    service.generateUniqueIdWithPrefix('app');
    service.generateUniqueIdWithPrefix('app');
    expect(service.getNumberOfGeneratedUniqueIds()).toBe(2);
  });

  it(`#${UniqueIdService.prototype.generateUniqueIdWithPrefix.name} SHOULD throw WHEN called with invalid`, () => {
    const invalidPrefixes = [null, undefined, '', '0', '1'];
    invalidPrefixes.forEach(
      invalid => expect(() => service.generateUniqueIdWithPrefix(invalid)).withContext(invalid).toThrow()) ;
  });
});
